# Movie Tracker

## Movie tracker

Movie tracker est un site qui permet d’obtenir un ensemble d’informations ( date de création, de sortie, sur quelle plateforme est-il disponible etc.) sur tous les films, séries et animés référencés rapidement et efficacement via son espace personnel sur le site. Dans ce site, les personnes ont la possibilité de personnaliser sa liste de contenu ( films, série et animés) déjà visualisée et de se créer une liste de contenu qu'elle aimerait voir. Les différents films, séries et animés sont récupérés à partir de L’[API](https://www.themoviedb.org/)

## Auteurs

Davia Moujabber
Kadir Akyar

## Installation

1. Cloonez le repository
2. Installez tous les packages avec la commande: npm install
3. Lancez le serveur avec la commande: npm run start:local

## Fonctionnalités

- Inscription et connexion pour un utilisateur
- Création de liste de films et de séries que nous avons déjà vu ou que nous souhaiterions voir.

## Routes de l'API

### Authenticification

- POST /users/signup : inscription des utilisations
- POST /users/login : connexion des utilisateurs
- GET /users/profile : renvoie le profile utilisateur

### Shows Watchlist

- GET /shows/watchlist : renvoie toutes les listes des séries à voir (pour les admins)
- POST /shows : créer une liste personnalisée de séries à voir
- PUT /show/watchlist/id : mettre à jour une série à voir
- GET /shows/watchlist/:id : récupérer la liste de série à voir avec le détail des séries

### Shows Watchedlist

- GET /shows/watchedlist : renvoie toutes les séries déjà vues (pour les admins)
- POST /shows/watchedlist : créer une liste personnalisée de séries déjà vu
- PUT /show/watchlist/id : mettre à jour une série déjà vu
- GET /shows/watchedlist/:id : récupérer la liste de série déjà vu avec le détail des séries

### Movies Watchlist

- GET /movies/watchlist : renvoie tous les films à voir (pour les admins)
- POST /movies/watchlist : créer une liste personnalisée de films à voir
- PUT /movies/watchlist/id : mettre à jour un film (ajouter ou supprimer) dans sa liste de films à voir
- GET / movies/watchlist/id : récupérer la liste de film à voir avec le détail des films

### Movies Watchedlist

- GET /movies/watchedlist : renvoie tous les films déjà vus (pour les admins)
- POST /movies/watchedlist : créer une liste personnalisée de films à voir
- PUT /movies/watchedlist/id : mettre à jour un film (ajouter ou supprimer) dans sa liste de films déja vue
- GET /movies/watchedlist/id : récupérer la liste de film déjà vu avec le détail des films

### Users (Admin)

- GET /users : renvoie tous les utilisateurs
- POST /users: créer un utilisateur
- DEL /users/id : supprimer un utilisateur
- GET /users/id : récupérer qu’un seul utilisateur

## Technologies utilisées

- Node.js
- Express
- MongoDB
- Postman

## Sécurité et performance

- HTTPS
- PassportJS(Jwt)
- Cors
- Helmet
- Rate Limiting
