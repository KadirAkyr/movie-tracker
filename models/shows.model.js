const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const showsSchema = new Schema(
  {
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    listOfShows: { type: Array, default: [] },
  },
  { collection: "shows", timestamps: true }
);

module.exports = mongoose.model("Show", showsSchema);
