const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    email: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    birthDate: { type: String },
    sexe: { type: String },
    isAdmin: { type: Boolean, default: false },
  },
  { collection: "users", timestamps: true }
);

module.exports = mongoose.model("User", userSchema);
