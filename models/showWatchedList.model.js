const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const watchedListSchema = new Schema(
  {
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    listOfShows: { type: Array, default: [] },
  },
  { collection: "showsWatchedList", timestamps: true }
);

module.exports = mongoose.model("ShowsWatchedList", watchedListSchema);
