const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const moviesSchema = new Schema(
  {
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    listOfMovies: { type: Array, default: [] },
  },
  { collection: "movies", timestamps: true }
);

module.exports = mongoose.model("Movies", moviesSchema);
