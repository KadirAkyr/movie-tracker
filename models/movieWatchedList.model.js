const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const watchedListSchema = new Schema(
  {
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    listOfMovies: { type: Array, default: [] },
  },
  { collection: "moviesWatchedList", timestamps: true }
);

module.exports = mongoose.model("MoviesWatchedList", watchedListSchema);
