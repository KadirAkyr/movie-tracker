// Mettre les URL, localhost port...
var config = {};

config.https = {
  keyPath: "./cert/key.pem",
  certPath: "./cert/cert.pem",
};

// cors whitlisted urls in prod and dev env
config.cors = {
  whitelist: {
    local: ["http://localhost:5678"],
    prod: [],
  },
};

// database uris for prod and dev env
config.mongoURI = {
  local: "mongodb://127.0.0.1:27017/movie-tracker",
  prod: "",
};

//rate limiting
config.rateLimit = {
  local: {
    windowMs: 60 * 1000, // 1 minute
    max: 100, // limit each IP to 100 requests per windowMs
    message: "Too many request please try again later",
  },
  prod: {
    windowMs: 60 * 1000, // 1 minute
    max: 100, // limit each IP to 100 requests per windowMs
    message: "Too many request please try again later",
  },
};

module.exports = config;
