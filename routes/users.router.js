const express = require("express");
const controller = require("../controllers/user.controller");
const router = express.Router();
const auth = require("../middlewares/auth.middleware");

router.get(
  "/profile",
  auth.isAuthenticated,
  auth.isSameUser,
  controller.profile
);
router.get("/:id", auth.isAuthenticated, auth.isAdmin, controller.getOne);
router.get("/", auth.isAuthenticated, auth.isAdmin, controller.getAll);
router.post("/signup", controller.signup);
router.post("/login", controller.login);
router.delete("/:id", auth.isAuthenticated, auth.isAdmin, controller.deleteOne);

module.exports = router;
