const express = require("express");
const controller = require("../controllers/shows.controller");
const router = express.Router();
const auth = require("../middlewares/auth.middleware");

router.get("/:id", auth.isAuthenticated, auth.isSameUser, controller.getOne);
router.get("/", auth.isAuthenticated, auth.isAdmin, controller.getAll);
router.post("/", auth.isAuthenticated, auth.isSameUser, controller.addOne);
router.put("/:id", auth.isAuthenticated, auth.isSameUser, controller.updateOne);

module.exports = router;
