const express = require("express");
const controller = require("../controllers/dataFromApi.controller");
const router = express.Router();

// Movies
router.get("/movie/popular", controller.populateHomePageMovie);
router.get("/movie/details/:id", controller.getOneMovieDetail);

// Shows
router.get("/show/popular", controller.populateHomePageShows);
router.get("/show/details/:id", controller.getOneShowDetail);

module.exports = router;
