const express = require("express");
const controller = require("../controllers/movies.controller");
const router = express.Router();
const auth = require("../middlewares/auth.middleware");

// Movies Watchlist
router.get("/:id", controller.getOne);
router.get("/", auth.isAuthenticated, auth.isAdmin, controller.getAll);
router.post("/", auth.isAuthenticated, auth.isSameUser, controller.addOne);
router.put("/:id", auth.isAuthenticated, auth.isSameUser, controller.updateOne);

module.exports = router;
