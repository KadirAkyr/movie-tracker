// DB Connection
require("./models/db");

var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const passport = require("passport");
const https = require("https");
const fs = require("fs");
const rateLimit = require("express-rate-limit");
const helmet = require("helmet");

// Modules
const config = require("./config");
var cors = require("./middlewares/cors.middleware");

const HTTPS = config.https;
const RATE_LIMIT = config.rateLimit.local;

// Routers
var usersRouter = require("./routes/users.router");
var showsRouter = require("./routes/shows.router");
var moviesRouter = require("./routes/movies.router");
var moviesWatchedList = require("./routes/movieWatchedList.router");
var showWatchedList = require("./routes/showWatcedList.router");
var dataFromApi = require("./routes/dataFromApi.router");

var app = express();

// Passport Config
require("./middlewares/passport")(passport);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(helmet());
app.use(cors);
app.use(rateLimit(RATE_LIMIT));

// Passport.js
app.use(passport.initialize());

// Routes
app.use("/users", usersRouter);
app.use("/shows/watchlist", showsRouter);
app.use("/movies/watchlist", moviesRouter);
app.use("/movies/watchedlist", moviesWatchedList);
app.use("/shows/watchedlist", showWatchedList);
app.use("/data", dataFromApi);

const sslServer = https.createServer(
  {
    key: fs.readFileSync(HTTPS.keyPath),
    cert: fs.readFileSync(HTTPS.certPath),
  },
  app
);

sslServer.listen(3443, () => {
  console.log("Secure server on port 3443");
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: err,
  });
});

module.exports = app;
