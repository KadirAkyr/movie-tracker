const passport = require("passport");

module.exports.isAdmin = async (req, res, next) => {
  if (!req.user.isAdmin) {
    // not authenticated, or authenticated incorrect
    err = new Error("Unauthorized ");
    err.status = 401;
    next(err);
  } else {
    next();
  }
};

module.exports.isAuthenticated = passport.authenticate("jwt", {
  session: false,
});

module.exports.isSameUser = async (req, res, next) => {
  const userId = req.user._id.toString();

  // Vérification pour les requêtes GET
  if (req.method === "GET" && req.params.id && req.params.id !== userId) {
    return res
      .status(401)
      .json({ error: "You are trying to get someone else data" });
  }

  // Vérification pour les requêtes POST
  if (req.method === "POST" && req.body.userId && req.body.userId !== userId) {
    return res
      .status(401)
      .json({ error: "You are trying to modify someone else data" });
  }

  next();
};
