const Show = require("../models/shows.model");
require("dotenv").config();
const axios = require("axios");

var controller = {
  getAll: async (req, res, next) => {
    try {
      let shows = await Show.find({}).populate("userId");
      res.json(shows);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  addOne: async (req, res, next) => {
    try {
      let show = await Show.create(req.body);
      res.json(show);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  deleteOne: async (req, res, next) => {
    try {
      let resp = await Show.findByIdAndDelete(req.params.id);
      res.json(resp);
    } catch (error) {
      console.log(error);
    }
  },

  getOne: async (req, res, next) => {
    try {
      let showList = await Show.findById(req.params.id);
      if (showList.listOfShows.length) {
        let tab = [];
        for (let index = 0; index < showList.listOfShows.length; index++) {
          const element = showList.listOfShows[index];
          try {
            let resp = await axios.get(
              "https://api.themoviedb.org/3/tv/" +
                element +
                "?api_key=" +
                process.env.API_KEY
            );
            tab.push(resp.data);
          } catch (error) {
            console.log(error);
            next(error);
          }
        }
        res.json({ showList: showList, ShowsDetail: tab });
      } else {
        res.json(showList);
      }
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  updateOne: async (req, res, next) => {
    try {
      let show = await Show.findById(req.params.id);
      const { showId } = req.body;
      if (show.listOfShows.includes(showId)) {
        const index = show.listOfShows.indexOf(showId);
        if (index > -1) show.listOfShows.splice(index, 1);
        await Show.findByIdAndUpdate(
          req.params.id,
          { listOfShows: show.listOfShows },
          {
            new: true,
          }
        );
      } else {
        show.listOfShows.push(showId);
        await Show.findByIdAndUpdate(
          req.params.id,
          { listOfShows: show.listOfShows },
          {
            new: true,
          }
        );
      }

      res.json(show);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  // Will serve to display movies in home page
  populateHomePage: async (req, res, next) => {
    try {
      let resp = await axios.get(
        "https://api.themoviedb.org/3/tv/popular?api_key=" + process.env.API_KEY
      );
      res.json(resp.data);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  getOneShowDetail: async (req, res, next) => {
    try {
      let resp = await axios.get(
        "https://api.themoviedb.org/3/tv/" +
          req.params.id +
          "?api_key=" +
          process.env.API_KEY
      );
      res.json(resp.data);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },
};

module.exports = controller;
