const Movie = require("../models/movies.model");
require("dotenv").config();
const axios = require("axios");

var controller = {
  getAll: async (req, res, next) => {
    try {
      let movies = await Movie.find({}).populate("userId");
      res.json(movies);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  addOne: async (req, res, next) => {
    try {
      let movie = await Movie.create(req.body);
      res.json(movie);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  deleteOne: async (req, res, next) => {
    try {
      let resp = await Movie.findByIdAndDelete(req.params.id);
      res.json(resp);
    } catch (error) {
      console.log(error);
    }
  },

  getOne: async (req, res, next) => {
    try {
      let movieList = await Movie.findById(req.params.id);
      if (movieList.listOfMovies.length) {
        let tab = [];
        for (let index = 0; index < movieList.listOfMovies.length; index++) {
          const element = movieList.listOfMovies[index];
          try {
            let resp = await axios.get(
              "https://api.themoviedb.org/3/movie/" +
                element +
                "?api_key=" +
                process.env.API_KEY
            );
            tab.push(resp.data);
          } catch (error) {
            console.log(error);
            next(error);
          }
        }
        res.json({ movieList: movieList, moviesDetail: tab });
      } else {
        res.json(movieList);
      }
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  updateOne: async (req, res, next) => {
    try {
      let movie = await Movie.findById(req.params.id);
      const { movieId } = req.body;
      if (movie.listOfMovies.includes(movieId)) {
        const index = movie.listOfMovies.indexOf(movieId);
        if (index > -1) movie.listOfMovies.splice(index, 1);
        await Movie.findByIdAndUpdate(
          req.params.id,
          { listOfMovies: movie.listOfMovies },
          {
            new: true,
          }
        );
      } else {
        movie.listOfMovies.push(movieId);
        await Movie.findByIdAndUpdate(
          req.params.id,
          { listOfMovies: movie.listOfMovies },
          {
            new: true,
          }
        );
      }

      res.json(movie);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  // Will serve to display movies in home page
  populateHomePage: async (req, res, next) => {
    try {
      let resp = await axios.get(
        "https://api.themoviedb.org/3/movie/popular?api_key=" +
          process.env.API_KEY
      );
      res.json(resp.data);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  getOneMovieDetail: async (req, res, next) => {
    try {
      let resp = await axios.get(
        "https://api.themoviedb.org/3/movie/" +
          req.params.id +
          "?api_key=" +
          process.env.API_KEY
      );
      res.json(resp.data);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },
};

module.exports = controller;
