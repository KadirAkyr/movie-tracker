const Movie = require("../models/movies.model");
require("dotenv").config();
const axios = require("axios");

var controller = {
  // Will serve to display movies in home page
  populateHomePageMovie: async (req, res, next) => {
    try {
      let resp = await axios.get(
        "https://api.themoviedb.org/3/movie/popular?api_key=" +
          process.env.API_KEY
      );
      res.json(resp.data);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  getOneMovieDetail: async (req, res, next) => {
    try {
      let resp = await axios.get(
        "https://api.themoviedb.org/3/movie/" +
          req.params.id +
          "?api_key=" +
          process.env.API_KEY
      );
      res.json(resp.data);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  // Will serve to display shows in home page
  populateHomePageShows: async (req, res, next) => {
    try {
      let resp = await axios.get(
        "https://api.themoviedb.org/3/tv/popular?api_key=" + process.env.API_KEY
      );
      res.json(resp.data);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  getOneShowDetail: async (req, res, next) => {
    try {
      let resp = await axios.get(
        "https://api.themoviedb.org/3/tv/" +
          req.params.id +
          "?api_key=" +
          process.env.API_KEY
      );
      res.json(resp.data);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },
};

module.exports = controller;
