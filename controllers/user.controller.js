const User = require("../models/user.model");
const MovieWatchedList = require("../models/movieWatchedList.model");
const MovieWatchList = require("../models/movies.model");
const ShowWatchedList = require("../models/showWatchedList.model");
const ShowWatchList = require("../models/shows.model");
const { check, validationResult } = require("express-validator");
const passport = require("passport");
const jwt = require("jsonwebtoken");

var controller = {
  getAll: async (req, res, next) => {
    try {
      let users = await User.find({});
      res.json(users);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  addOne: async (req, res, next) => {
    try {
      let user = await User.create(req.body);
      res.json(user);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  getOne: async (req, res, next) => {
    try {
      let user = await User.findById(req.params.id);
      res.json(user);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  updateOne: async (req, res, next) => {
    try {
      let user = await User.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
      });
      res.json(user);
    } catch (error) {
      console.log(error);
      next(error);
    }
  },

  deleteOne: async (req, res, next) => {
    try {
      let resp = await User.findByIdAndDelete(req.params.id);
      res.json(resp);
    } catch (error) {
      console.log(error);
    }
  },

  // Get all data for front profile page
  profile: async (req, res, next) => {
    try {
      let me = await User.findOne({ user: req.user._id });
      let movieWatchedList = await MovieWatchedList.findOne({
        user: req.params.id,
      });
      let movieWatchList = await MovieWatchList.findOne({
        user: req.params.id,
      });
      let showWatchedList = await ShowWatchedList.findOne({
        user: req.params.id,
      });
      let showWatchList = await ShowWatchList.findOne({
        user: req.params.id,
      });
      res.json({
        user: {
          id: me.id,
          email: me.email,
          username: me.username,
          accountCreationTime: me.createdAt,
        },
        movieWatchedList: {
          id: movieWatchedList._id,
        },
        movieWatchList: {
          id: movieWatchList._id,
        },
        showWatchedList: {
          id: showWatchedList._id,
        },
        showWatchList: {
          id: showWatchList._id,
        },
      });
    } catch (error) {
      console.log(error);
    }
  },

  login: async (req, res, next) => {
    passport.authenticate("login", async (err, user, info) => {
      try {
        if (err || !user) {
          const error = new Error("An error occurred.");

          return next(error);
        }

        req.login(user, { session: false }, async (error) => {
          if (error) return next(error);

          const body = {
            _id: user._id,
            email: user.email,
            isAdmin: user.isAdmin,
          };
          const token = jwt.sign({ user: body }, "TOP_SECRET");

          return res.json({ token });
        });
      } catch (error) {
        return next(error);
      }
    })(req, res, next);
  },

  signup: async (req, res, next) => {
    [
      check("username", "Please Enter a Valid Username").not().isEmpty(),
      check("email", "Please enter a valid email").isEmail(),
      check("password", "Please enter a valid password").isLength({
        min: 6,
      }),
    ];
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }

    const { username, email, password } = req.body;
    try {
      let user = await User.findOne({
        email,
      });
      if (user) {
        return res.status(400).json({
          msg: "User Already Exists",
        });
      }

      user = new User({
        username,
        email,
        password,
      });

      await user.save();
      res.status(200).send({ message: "User registered", user: user });
    } catch (err) {
      console.log(err.message);
      res.status(500).send("Error in registering");
    }
  },
};
module.exports = controller;
